import sys
import numpy as ny
import matplotlib.pyplot as plt
import time

#funciones para saber si tenemos un turno en CCW
def CCW(p1,p2,p3):
    if (p3[1]-p1[1])*(p2[0]-p1[0])>=(p2[1]-p1[1])*(p3[0]-p1[0]):
        return True
    return False

#Funciones principales
def GiftWrapping(S): # envoltorio de regalos
    plt.figure() #definimos la figura
    index=0
    n=len(S)
    p=[None]*n
    l=ny.where(S[:,0]==ny.min(S[:,0]))
    pointonHull=S[l[0][0]]
    i=0
    while True:
        p[i]=pointonHull
        endpoint=S[0]
##Se ordenana los puntos en forma antihoraria
        for j in range(1,n):
            if(endpoint[0]==pointonHull[0] and endpoint[1]==pointonHull[1])or not CCW(S[j],p[i],endpoint):
                endpoint=S[j]
        i=i+1
        pointonHull=endpoint
        #J=ny.array([p[k] for k in range(n) if p[k] is not None])
        #plt.clf() #clear plot
        #plt.plot(J[:,0],J[:,1],'b-',pickradius=5) # plotear la lineas
        #plt.plot(S[:,0],S[:,1],".r") # puntos de trazado rojos
        #plt.axis('off')# sin ejes
        #plt.show(block=False) #cerrar la grafica
        #plt.pause(0.0001) #minipausa antes de cerrar la grafica
        #index+=1
        if endpoint[0]==p[0][0] and endpoint[1]==p[0][1]:
            break
    for i in range(n):
        if p[-1] is None:
                del p[-1]
    p=ny.array(p)
    # impresion del casco final
    plt.clf()
    plt.plot(p[:,0],p[:,1],'-b',pickradius=5)
    plt.plot([p[-1,0],p[0,0]],[p[-1,1],p[0,1]],'b-',pickradius=5)
    plt.plot(S[:,0],S[:,1],".r")
    plt.axis('off')
    plt.show(block=False)
    plt.pause(0.0000001)
    return p

##Calcular la ecuación de la recta que pasa por a y b, esto es l
##Calcular la distancia entre l y c
def distanciaRP(a, b, c):
    m = (b[1] - a[1])/(b[0] - a[0])
    A = -m
    B = 1
    C = -(a[1]-a[0]*m)
    d = (ny.abs(A*c[0] + B*c[1] + C))/ny.sqrt(A**2 + B**2)
    return d

##Se calcula el área de un triángulo
def area(a, b, c):
    R = 0.5 * (a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[0] - b[1]))
    R = ny.abs(R)
    return R

##Se toman dos vértices y se calcula la distancia a cada punto
vector = []
areas = []
def distancias(S):
    for i in range(len(S)):
        a = S[i]
        if i == len(S)-1:
            b = S[0]
        else:
            b = S[i + 1]

        aux = 0
        aux1 = 0
        for j in range(len(S)):
            c = S[j]
            if (j != i) and (j != i+1):
                d = distanciaRP(a, b, c)
                e = area(a, b, c)
                if d > aux:
                    aux = d
                if e > aux1:
                    aux1 = e
        vector.append(aux)
        areas.append(aux1)

def main ():
    start_time = time.time()

    """try:
        N=int(sys.argv[1])  #Retorna una lista con todos los argumentos pasados por línea de comandos
    except:
        N=int(input("Introduce la N: "))"""

    N= 1000
    # Por defecto construimos un conjunto aleatorio de N puntos con coordenadas en [0,300) x [0,300):
    p=ny.array([(ny.random.randint(0,300),ny.random.randint(0,300)) for i in range(N)])
    #Se ingresan los puntos, para calcular el envovente convexo
    p = ny.array([(-2, 72), (-71, 13), (17, -70), (9, 10), (-24, 67), (73, 60), (-37, -77), (54, 2), (78, -15), (-41, -47),
         (-85, -25), (70, -28)])

    L = GiftWrapping(p)
    distancias(L)
    minV = ny.amin(vector)
    minA = ny.amin(areas)
    print('El ancho del robot es:', minV)
    print('Menor área:', minA)

    time_taken = time.time() - start_time

    # print("Generated a {} sided convex hull to contain {} points".format(poly.sides, number_of_points))
    print("Time taken: {}s".format(time_taken))

    # Usamos la figura predefinida
    plt.plot(L[:,0],L[:,1],'or',pickradius=5)
    plt.plot([L[-1,0],L[0,0]],[L[-1,1],L[0,1]],'b-',pickradius=5)
    plt.plot(p[:,0],p[:,1],".r")
    plt.axis('off')
    plt.show()

    return time_taken

if __name__=='__main__':
    main()

