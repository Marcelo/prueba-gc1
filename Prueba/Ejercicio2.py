import sys
import numpy as ny
import matplotlib.pyplot as plt
import time

#funciones para saber si tenemos un turno en CCW
def CCW(p1,p2,p3):
    if (p3[1]-p1[1])*(p2[0]-p1[0])>=(p2[1]-p1[1])*(p3[0]-p1[0]):
        return True
    return False

#Funciones principales
def GiftWrapping(S): # envoltorio de regalos
    plt.figure() #definimos la figura
    index=0
    n=len(S)
    p=[None]*n
    l=ny.where(S[:,0]==ny.min(S[:,0]))
    pointonHull=S[l[0][0]]
    i=0
    while True:
        p[i]=pointonHull
        endpoint=S[0]
##Se ordenana los puntos en forma antihoraria
        for j in range(1,n):
            if(endpoint[0]==pointonHull[0] and endpoint[1]==pointonHull[1])or not CCW(S[j],p[i],endpoint):
                endpoint=S[j]
        i=i+1
        pointonHull=endpoint
        #J=ny.array([p[k] for k in range(n) if p[k] is not None])
        #plt.clf() #clear plot
        #plt.plot(J[:,0],J[:,1],'b-',pickradius=5) # plotear la lineas
        #plt.plot(S[:,0],S[:,1],".r") # puntos de trazado rojos
        #plt.axis('off')# sin ejes
        #plt.show(block=False) #cerrar la grafica
        #plt.pause(0.0001) #minipausa antes de cerrar la grafica
        #index+=1
        if endpoint[0]==p[0][0] and endpoint[1]==p[0][1]:
            break
    for i in range(n):
        if p[-1] is None:
                del p[-1]
    p=ny.array(p)
    # impresion del casco final
    plt.clf()
    plt.plot(p[:,0],p[:,1],'-b',pickradius=5)
    plt.plot([p[-1,0],p[0,0]],[p[-1,1],p[0,1]],'b-',pickradius=5)
    plt.plot(S[:,0],S[:,1],".r")
    plt.axis('off')
    plt.show(block=False)
    plt.pause(0.0000001)
    return p

##Calcular la ecuación de la recta que pasa por a y b, esto es l
##Calcular la distancia entre l y c
def distanciaRP(a, b, c):
    m = (b[1] - a[1])/(b[0] - a[0])
    A = -m
    B = 1
    C = -(a[1]-a[0]*m)
    d = (ny.abs(A*c[0] + B*c[1] + C))/ny.sqrt(A**2 + B**2)
    return d

##Calcula recta a partir de dos puntos
def rectaPuntos(a, b):
    m = (b[1] - a[1]) / (b[0] - a[0])
    A = -m
    B = 1
    C = (a[1] - a[0] * m)
    return A,B,C

##Calcula la pendiente entre dos puntos
def pendiente(a,b):
    m = (b[1] - a[1]) / (b[0] - a[0])
    return m

##Calcula la recta a partir de un punto y una pendiente
def rectaPendiente(a, m):
    A = -m
    B = 1
    C = (a[1] - a[0] * m)
    return A, B, C

##Se calcula el área de un triángulo
def area(a, b, c):
    R = 0.5 * (a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[0] - b[1]))
    R = ny.abs(R)
    return R
##Se calcula la distancia entre dos puntos
def distanciaP(a,b):
    x = (b[0] - a[0]) ** 2
    y = (b[1] - a[1]) ** 2
    R = ny.sqrt(x + y)
    return R

##Se toman dos vértices y se calcula la mayor área
vector = []
areas = []
def distancias(S, n):
    a = S[n]
    b = S[n+1]

    aux1 = 0
    j_aux = 0
    for j in range(len(S)):
        c = S[j]
        if (c[0] != a[0]) and (c[0] != b[0]):
            e = area(a, b, c)
            if e > aux1:
                aux1 = e
                j_aux = j

    areas.append(aux1)
    vector.append(j_aux)

##Se calcula la distancia máxima, y se obtienen sus puntos
dis = []
vector_i = []
vector_j = []
vector_max = []
def distanciaM(S):
    for i in range(len(S)):
        max = 0
        i_aux = 0
        j_aux = 0
        for j in range(len(S)):
            if j != i:
                a = S[i]
                b = S[j]
                R = distanciaP(a,b)
                if R > max:
                    i_aux = i
                    j_aux = j
                    max = R
        vector_i.append(i_aux)
        vector_j.append(j_aux)
        vector_max.append(max)

##Se determina si los puntos están a la derecha izquierda o dentro de un triángulo
VectorEC  = []
VectorECI = []
VectorECD = []
def triangulo(a,b,c, S):
    for i in range(len(S)):
        d = S[i]

        if (d[0] != a[0]) and (d[0] != b[0]) and (d[0] != c[0] and
            d[1] != a[1]) and (d[1] != b[1]) and (d[1] != c[1]):
            ax_1, ay_1 = a[0], a[1]
            ax_2, ay_2 = c[0], c[1]
            ax_3, ay_3 = b[0], b[1]
            In_X, In_Y = d[0], d[1]
            R1 = 0.5 * (ax_1 * (ay_2 - In_Y) + ax_2 * (In_Y - ay_1) + In_X * (ay_1 - ay_2))
            R2 = 0.5 * (ax_3 * (ay_2 - In_Y) + ax_2 * (In_Y - ay_3) + In_X * (ay_3 - ay_2))
            if R1 > 0:
                VectorECI.append(d)
            if R2 < 0:
                VectorECD.append(d)
            if R1 < 0 and R2 > 0:
                VectorEC.append(d)
##Esta función calcula la proyección de ECI y ECD, y calcula la distancia máxima
def proyeccion(I, D, a, b):
    x1, x2, x3 = rectaPuntos(a,b)
    m = pendiente(a,b)
    m1 = -(1/m)
    ##Lado Izquierdo
    auxI=0
    for i in range(len(I)):
        c = I[i]
        y1, y2, y3 = rectaPendiente(c, m1)
        ##Se define una matriz para calcular el punto de corte
        A = ny.array([[x1, x2], [y1, y2]])
        B = ny.array([[x3], [y3]])
        x = ny.linalg.inv(A).dot(B)
        ##Punto de corte
        z = [x[0,0], x[1,0]]
        a1 = distanciaP(z, a)
        if a1 > auxI:
            auxI = a1
    print('=====================')
    print('Distancia a1 =',auxI)
    print('=====================')
    ##Lado Derecho
    auxD = 0
    for i in range(len(D)):
        c = D[i]
        y1, y2, y3 = rectaPendiente(c, m1)
        ##Se define una matriz para calcular el punto de corte
        A = ny.array([[x1, x2], [y1, y2]])
        B = ny.array([[x3], [y3]])
        x = ny.linalg.inv(A).dot(B)
        ##Punto de corte
        z = [x[0, 0], x[1, 0]]
        a2 = distanciaP(z, b)
        if a2 > auxD:
            auxD = a2
    print('Distancia a2 =',auxD)
    print('=====================')
    a3 = distanciaP(a, b)
    print('Distancia a3 =',a3)

    return auxI + auxD + a3


#Coordenadas de los puntos máximos en S
i_max = 0
j_max = 0
def main ():
    start_time = time.time()

    """try:
        N=int(sys.argv[1])  #Retorna una lista con todos los argumentos pasados por línea de comandos
    except:
        N=int(input("Introduce la N: "))"""

    N= 1000
    # Por defecto construimos un conjunto aleatorio de N puntos con coordenadas en [0,300) x [0,300):
    p=ny.array([(ny.random.randint(0,300),ny.random.randint(0,300)) for i in range(N)])
    #Se ingresan los puntos, para calcular el envovente convexo
    p = ny.array([(-2, 72), (-71, 13), (17, -70), (9, 10), (-24, 67), (73, 60), (-37, -77), (54, 2), (78, -15), (-41, -47),
         (-85, -25), (70, -28)])
# -------------------------------
    #Literal a
    L = GiftWrapping(p)
# -------------------------------
    #Literal b
    distanciaM(L)
    ##Se define el diámetro mayor
    max = ny.amax(vector_max)
    pos = ny.where(vector_max == max)
    pos = ny.array(pos[0])
    i_max = vector_i[pos[0]]
    j_max = vector_j[pos[0]]
    print('puntos que tienen el diámetro mayor', L[i_max], L[j_max])
# --------------------------------
    #Literal c
    distancias(L, i_max)
    ##Se calcula el punto con el que se crea el área mayor
    max = ny.amax(areas)
    pos = ny.where(areas == max)
    pos = ny.array(pos[0])
    k_max = vector[pos[0]]
    print('punto con el que se forma el área mayor', L[k_max], ' área:', max)
# --------------------------------
    # Literal d
    triangulo(L[i_max], L[i_max+1], L[k_max], L)
    ##Se separan los puntos en EC, ECI, ECD
    ECI = ny.array(VectorECI)
    ECD = ny.array(VectorECD)
    print('Los elementos a la izquierda son:', ECI[0], ECI[1], 'Los elementos a la derecha son:', ECD[0], ECD[1], ECD[2],ECD[3])
# -------------------------------
    #Literal e
    R = proyeccion(ECI, ECD, L[i_max], L[i_max+1])
    ##Se calculan las proyecciones y distancias máximas
    print('El lado rectángulo es:', R)
    time_taken = time.time() - start_time

    # print("Generated a {} sided convex hull to contain {} points".format(poly.sides, number_of_points))
    print("Time taken: {}s".format(time_taken))

    # Usamos la figura predefinida
    plt.plot(L[:,0],L[:,1],'or',pickradius=5)
    plt.plot([L[-1,0],L[0,0]],[L[-1,1],L[0,1]],'b-',pickradius=5)
    plt.plot(p[:,0],p[:,1],".r")
    plt.axis('off')
    plt.show()

    return time_taken

if __name__=='__main__':
    main()

